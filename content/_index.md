---
title: "Home"
---

Hi! I'm Austin DeLauney, and my internet alias is 'laundry(96/_96/-96)'.

This is my personal website that showcases my work and outlines my career. If you'd like to get in contact with me about questions about my work, or some help with something I made, [please do!](contact/).

I originally had a website that was made with pure HTML5 and CSS3, but have since moved over to [Hugo](https://gohugo.io/), and I am using the [cupper theme](https://github.com/zwbetz-gh/cupper-hugo-theme).

Head on over to my blog posts to see what my thoughts are on some things, or hear me talk about my personal projects!

Outside of those posts, some things I enjoy to spend my spare time doing are DMing for my small group of friends, working on my [wallpaper generator](https://gitlab.com/laundry-96/pape-gen), working on this website, snapping some photos, and watching various TV shows and YouTube channels.
