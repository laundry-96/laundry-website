---
title: "Thinning a Rust Binary, PT 1"
date: 2018-10-30T00:00:00-04:00
tags: [rust]
---
 
Pape-Gen is one of my pet projects, and I was thinking of creating a WASM build and hosting it on this website, however, I realized that the current binary is whopping 1.5MB! A whole 1.5 MB just to generate some lines for a picture? No thank you, so today marks the day I try to 'thin' Pape-Gen to something lower than that. I would like to see 500 KB max, but I don't know if that will work... So let's get started!!

The first thing I did was install and run cargo tree, as it gave me a list of all the dependecies I used. With this, I could see which dependencies I used, and which dependencies those dependecies used, and so on. The image crate was using a lot of dependencies. Because of this I started researching an alternative for creating and writing PNG files (image also has support for many image formats), and stumbled into a thing called 'features'.

Features are specified here in the cargo documentation. From what I can gather, they allow a project to be split into standalone parts, and be used as a single part, however when all of the parts are together, they can allow for a more ‘complete’ package.

Anyways, specifying that I just wanted the ‘PNG’ feature of the crate brought my final executable down from 1.5 MB to 1.4 MB. Not as much as I was hoping, but it was definetly a start!

My next adventure is to go into the target/release/deps folder and see which dependencies had the biggest size. Running `ls -lahS`, I am able to sort the files by size and the biggest file happened to be libclap weighing in at a hefty 4.9 MB. Quite outragous if you ask me. In order to start digging, I cloned the clap repository and built it for release. The size of the release rlib is 3.5 MB... YIKES.

In the next installment, I will be replacing StructOpt with CLAP, and see if that yields any improvements 
