---
title: Using Jinja to Template HTML
date: 2020-05-06T00:00:00-04:00
tags: [jinja2, website]
---

Upon creating this website, I have realized that there is a lot of boilerplate code that I reuse often. I thought copying and pasting the same snippets would work well, but eventually that didn't pan out the way I thought it would. If I wanted to make a small change to my boilerplate, I would have to replace that snippet in EVERY html file...

For some time I have been using SaltStack as a configuration management tool, and it has support for loading variables for use with templating. The templating technology in use is [Jinja](https://jinja.palletsprojects.com/en/2.11.x/). Jinja is a very versitile tool, and can handle extremely complex situations, of which I will not go into depth here. Since Jinja was the only templating technology I've ever encountered, I decided to roll my sleeves up and get working.

Within minutes, I had written Jinja into my HTML files, but unfortanetly I had no way to generate all files at once, since I was using a [simple example](https://youtu.be/7mdBRSBS5vc). I then decided I could modify the app.py and use it to find all "html" files, which would have Jinja, and render them one at a time, and place them into a folder called "rendered". This allowed me to have my Gitlab pipeline deploy the proper HTML files without changing anything.

Templating like this allows me to make edits to the websites design without having to edit every single file. If you are interested in how I did it, please feel free to take a look at the websites source code at my Gitlab page! Once again, thanks for reading folks. Until next time! 
