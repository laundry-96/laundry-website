---
title: Creating Files in the /run Directory
date: 2019-06-29T00:00:00-04:00
tags: [rpm, gunicorn]
---

Things that suck: Creating an RPM, only to not have it write files you need. In this sense, I mean the fact that I need to create a directory in /run for /run/gunicorn, and I specify in my SPEC file for an RPM to create the folder $RPM_ROOT_BUILD/run/gunicorn, yet it doesn't get installed on the system.

After countless hours trying to figure out why the folder /run/gunicron will not be created, (The RPM knows it should be created, as the folder is packaged in the RPM, and when I run 'rpm -V package_name', it says 'missing: /run/gunicorn) I realized that this can be attributed to the /run directory being a temporary file system (tmpfs), and wiping everything after reboot.

The proper fix for this problem is by using [tmpfiles.d](https://www.freedesktop.org/software/systemd/man/tmpfiles.d.html) (located at /etc/tmpfiles.d). Create a file in this directory called package_name.conf (in my case, since my unit file for systemd was gunicorn.service, I name dit gunicorn.conf). The contents of this file should list out all the files that need to be created when starting and running the service.

That's it for now folks! If you have any questions/comments please feel free to email/tweet me! 
