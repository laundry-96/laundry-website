---
title: About Me
---

I _love_ software. I love looking at a project, seeing how it works, reading documentation, writing documentation, submitting bug reports, writing fixes and features. I love seeing the project mature into something that is used by a lot of people, and also by a few people. I love the process, the tinkering, the problems, the people, and the community. I *_love_* software.

## My First Foray into Computers
### Python
I started my career out in at my grandmother's house in 7th grade. My uncle came to me and noticed I was playing games, and he (I kid you not) told me to do something meaningful like learn Python. At this point, I could continue playing Club Penguin, or search for, what I thought was at the time, a snake program. I googled it, and like many others have come across, loaded up [TheNewBoston](https://www.youtube.com/user/thenewboston) and started learning Python 3.

### Operating Systems
Soon after learning a bit of Python, I read about a silly little thing called "Linux". I was hooked. I installed VM and fiddled around with some distrobutions:
* (X/L/K)Ubuntu
* Fedora
* OpenSuSe
* CrunchBang (One of my all time favs)

### C/C++
In 8th grade I started fiddling around with C & C++ after learing about Zork. I never really played these games extensively, but I was interested and wanted to attempt to remake it in C/C++. This project never really took off, because C/C++ was never really made for these kinds of projects, that's what functional programming is for. Nonetheless, I understood the syntax and C++'s extra bells and whistles even though I never really got to use them.

### C#
Since my Zork project didn't really take off, and I still wanted to make some sort of 'game', I stumbled across C# and XNA (RIP). I started googling around for some projects and examples, and there was a Space Invaders clone, which I decided to replicate and attempt to go at myself. I was able to see the aliens fly across the screen and have the bullets hit them and kill them.

### Forums
Through my searches on the internet, I found out about a cool couple forums (LinusTechTips, Codeplex, Toms Hardware) and joined their communities, posted, and even helped people solve problems. I'd spend at least a couple of hours a day just watching the posts and replying and what not.

### Reverse Engineering
I'm not sure how I came across a Carniggie Binary Bomb, but I was beyond interested, and I learned some key linux tools like strace, and how system calls work, and also I became very familiar with GDB and inserting an extreme amount of breakpoints to make sure I would stop at the correct place.

### Highschool Classes
I never really had an issue with my computer science classes in Highschool because they were all in Java, but they did help me understand the whole point of OOD, which was encapsulation, polymorphism, inheritance. Believe it or not, when I was writing my Zork clone, I thought it was a good idea to have one file full of fields, one file full of methods, and another file that had the logic of the game calling the methods and fields from the aforementioned files.

In Computer science 3, 4, and Advanced Object Oriented Design, I had my bearings straight and was able to complete the projects with relative ease (except CS 4 projects took a while, it was basically a Data Structures class).

## College Years

### Freshmen year
I took Computer Science 202, which was all in C++, so attempting to write that Zork clone helped me :). The projects in 202 were a ton of fun because of the nights I'd stay up with my friends and work on the projects the night they came out. We would all get together and have snacks before disecting the prompts for each project to figure out which direction we should take the project in.

### Sophomore Year
The two major classes that defined this year was Data Science, and Assembly. I thought back in 202, I had late nights staying up doing the projects, but I stayed up even later working on some of these projects (most notibly a project using 2-3 B Trees), and working on them even up until the deadline. For the Assembly class, I was thankful for taking my time and playing with the binary bomb so I was at least able to handle myself after getting thrown into the deep end. We learned about moving registers around and how to actually make function calls in assembly. Super neat stuff.

### Junior Year
Two things defined my Junior year at UMBC, and that was having my own radio show for a semester called "Graveyard Shift" where I just read posts off of Reddit's r/nosleep and r/creepypasta. I would wrap up late, so this is when I started getting addicted to chocolate covered coffee beans. The second thing that defined my Junior year was CS 421 - Operating Systems. This class was my favorite class in College. *PERIOD*. One of our projects was writing a shell, and using the functions exec, and execve. One of the project was creating a message passing interface in the kernel, and writing that was A TON of fun. Even more late nights delving into the depths of the Linux kernel to figure out how to load something at boot.

### Senior Year
I took Robotics and wrote a maze wandering function for a robot, and me and my group would basically meet up every day trying to get this robot to work. It didn't really help that we were all bad at basic calculus (sin/cosine for turning) and would constanty do guess work until one of our teammates actually wrote code using sin and cosine. I also took Algorithms, and as much as I learned in this class, it wasn't my favorite. Either because there were a lot of ways I'd want to do something, only to learn that it was wrong, or because you'd have to memorize every single algorithm and how they worked (and there were a lot with small variances and it was annoying. Looking at you path finding!).

## Career

### Center of Advanced Study of Language (CASL)
I worked at CASL (at University of Maryland, College Park) in the summer of Junior to Senior year of highschool. I wrote some Java code in Spring Boot to join various definitions for parts of speech for an Arabic word into a single entry.

### Koolspan
I started my career at Koolspan in the summer of 2015, after Freshmen year of college. I worked on an assortment of projects at Koolspan, from the iOS app to a backend service to pipelines!
* ELK Stack
  * Installed and setup Elasticsearch, Kibana, and Logstash
  * Wrote a parser in Java to convert basic INI file generated by the Blackberry, Android and iOS app and transformed that data into JSON to import to ELK
  * Wrote Logstash grok rules for the different mobile platforms
* User Aggregator
  * Converted a bash script to grab files from various servers over SFTP, and put them into a single file
  * Wrote this in Python 2 (then ported to 3) and added functionality for error handling and installation
  * The installation portion of the script was triggered by passing in the required arguments, and depending if the server was supposed to have a node in the network or a master
    * The node creates a user and an sftp jail for the master to grab it's files for processing by the master
    * The master creates a user and queries all of the sftp sites for placing keys in known_hosts, and aggregates all data into a single file to be sent to another backend service
  * Packaged the application with pip, so installing the file was as easy as running pip install _package_

### Accenture

