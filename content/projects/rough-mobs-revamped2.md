---
title: Rough Mobs Revamped 2
---

Rough Mobs Revamped is a mod that enhances the experience of Minecraft by increasing the difficulty of Minecraft. It does this by:
* Armor for mobs
* "Boss" mobs (mobs with extreme properties)
* Increased logic
  * Path finding
  * Breaking and placing blocks

I'm currently working on this mod because I find the difficulty of Minecraft is too 'easy', in the sense that mobs are just moderate dangers. I want the experience that everything can kill you, and make this game super hard.
