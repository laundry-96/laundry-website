---
title: pape-gen
---

One of my favorite projects that I've worked on is [pape-gen](https://gitlab.com/laundry-96/pape-gen). One day I really got into the idea of generative art, and I wanted to get better at Rust, so I decided to start working on this. I would like to eventually submit a proposal to a convention and speak on behalf of my program, so I will have to keep at this.

## Inspiration
* [Isaac Grosof](https://isaacg1.github.io/2018/12/06/programmatically-generated-artwork.html)
* [Why Love Generative Art by Jason Bailey](https://www.artnome.com/news/2018/8/8/why-love-generative-art)
* [A bunch of generative art samples](https://github.com/anaulin/generative-art)

## Examples

TODO: Place examples here
